package com.demo.studyqiniu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyQiniuApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyQiniuApplication.class, args);
    }

}
