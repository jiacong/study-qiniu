package com.demo.studyqiniu.controller;

import com.demo.studyqiniu.pojo.Result;
import com.demo.studyqiniu.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @description:
 * @author: jiacong
 * @time: 2020/6/8 10:59
 */
@RestController
@RequestMapping("file")
public class FileController {

    @Autowired
    IFileService iFileService;

    @PostMapping("upload")
    public Result uploadFile(@RequestParam(value = "file") MultipartFile file) {
        try {
            String upload = iFileService.upload(file.getBytes());
            return Result.success(upload);
        } catch (IOException e) {
            e.printStackTrace();
            return Result.fail("上传异常:" + e.getMessage());
        }
    }
}
