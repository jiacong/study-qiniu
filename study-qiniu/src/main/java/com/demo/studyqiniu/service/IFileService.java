package com.demo.studyqiniu.service;

import com.qiniu.common.QiniuException;

public interface IFileService {

    /**
     * 上传文件到七牛云
     * @param uploadBytes
     * @return 访问url
     * @throws QiniuException
     */
    String upload(byte[] uploadBytes) throws QiniuException;
}
