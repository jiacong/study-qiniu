package com.demo.studyqiniu.pojo;

import cn.hutool.http.HttpStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: jiacong
 * @time: 2020/6/8 11:02
 */
@Data
public class Result implements Serializable {

    /**
     * 200成功，其他失败
     */
    private int code;
    /**
     * 提示信息
     */
    private String msg;
    /**
     * 返回数据
     */
    private Object data;


    public static Result success() {
        return Result.success("操作成功", null);
    }

    public static Result success(Object data) {
        return Result.success("操作成功", data);
    }

    public static Result success(String msg, Object data) {
        Result result = new Result();
        result.code = HttpStatus.HTTP_OK;
        result.msg = msg;
        result.data = data;
        return result;
    }

    public static Result fail(String msg) {
        Result result = new Result();
        result.code = HttpStatus.HTTP_INTERNAL_ERROR;
        result.data = null;
        result.msg = msg;
        return result;
    }
}
